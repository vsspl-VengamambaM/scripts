package transaction.script.platform.startup;

import transaction.script.models.MasterCollectionTo;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;

public class MongodbTesting {
	/**
	 * {
	 * 
"_id":"57e197c4e4b07df427ce4a60",
"name":"THIRUMALA FERTILIZERS",
"bizName":"THIRUMALA FERTILIZERS",
"bizTypeId":"BT000008",
"locationId":"33500300050004",
"mobileNum":"+910000004389",
"countryCode":"+91",
"status":"IN_QUEUE",
"prfCreatedDate":"2016-09-22T09:51:27Z"
}
	 * @param args
	 * @throws Exception
	 */

	public static void main(String[] args) throws Exception {
		DB db = MongoDBUtil.getConnection();
		DBCollection coll = db.getCollection("masterCollectionTest");
		MasterCollectionTo obj = new MasterCollectionTo();
		obj.setMobileNum("+910000004389");
		obj.setStatus("TEST");
		//masterCollection.update(new BasicDBObject("mobileNum", objMasterCollectionTo.getMobileNum()), gson.fromJson(gson.toJson(objMasterCollectionTo), BasicDBObject.class));
		coll.update(new BasicDBObject("mobileNum",obj.getMobileNum()), new Gson().fromJson(new Gson().toJson(obj), BasicDBObject.class));
		System.out.println("end of lineeeeeee ***************************");
		/*DBCursor cursor = coll.find(new BasicDBObject("mobileNum","+910000004389"));
		while(cursor.hasNext()){
			//MasterCollectionTo obj = new Gson().fromJson(cursor.next().toString(), MasterCollectionTo.class);
			System.out.println("data is " + new Gson().toJson(obj));
		}*/
	}
}
