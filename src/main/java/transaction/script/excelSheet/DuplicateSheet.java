package transaction.script.excelSheet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class DuplicateSheet {
	public static final Logger LOGGER = LoggerFactory.getLogger(DuplicateSheet.class);
	static int lastrowCount = 10;
	static DB db = null;
	static DBCollection testCollection = null;
	static Gson gson = null;
	static {
		try {
			db = MongoDBUtil.getConnection();
			testCollection = db.getCollection("TestCollection");
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("Unable to connect to database");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		int limitCount;
		int skipCount;
		BasicDBObject searchQuery = new BasicDBObject();
		BasicDBList statusList = new BasicDBList();
		statusList.add(AppConstants.DUPLICATE_ALL);
		statusList.add(AppConstants.DUPLICATE_DATA);
		statusList.add(AppConstants.DUPLICATE_MOBILE);
		DBObject inClause = new BasicDBObject("$in", statusList);
		searchQuery.put("status", inClause);
		DBCursor dbCursor = testCollection.find(searchQuery).limit(20).skip(10);
		while (dbCursor.hasNext()) {
			String dbObject = null;
			dbObject = dbCursor.next().toString();
			ExcelTo objExcelTo = gson.fromJson(dbObject, ExcelTo.class);
			System.out.println(gson.toJson(objExcelTo));
			if (objExcelTo.getStatus().equalsIgnoreCase(AppConstants.DUPLICATE_ALL)) {
				WritingExcelDuplicateAll(objExcelTo);
			}
			if (objExcelTo.getStatus().equalsIgnoreCase(AppConstants.DUPLICATE_DATA)) {
				WritingExcelDuplicateData(objExcelTo);
			}
			if (objExcelTo.getStatus().equalsIgnoreCase(AppConstants.DUPLICATE_MOBILE)) {
				WritingExcelDuplicatePH(objExcelTo);

			}

		}

		System.out.println("111111");
	}

	public static void WritingExcelDuplicatePH(ExcelTo objExcelTo) throws Exception {
		String duplicatePhNo = "/home/vasudhaika/duplicatePhNo.xlsx";
		File duplicatePhNoFile = new File(duplicatePhNo);
		Workbook workbook;
		Sheet sheet;
		int rowCount;
		if (duplicatePhNoFile.exists()) {
			FileInputStream fis = new FileInputStream(duplicatePhNoFile);
			workbook = new XSSFWorkbook(fis);
			sheet = workbook.getSheetAt(workbook.getNumberOfSheets() - 1);
			rowCount = sheet.getLastRowNum() + 1;
			if (rowCount == lastrowCount) {
				int sheetNumber = workbook.getNumberOfSheets() + 1;
				sheet = workbook.createSheet("Sheet" + sheetNumber);
				rowCount = 0;
			}
		} else {
			workbook = new XSSFWorkbook();
			sheet = workbook.createSheet("Sheet1");
			rowCount = 0;
		}
		Row row;
		String arr[] = new String[] { objExcelTo.getName(), objExcelTo.getBusinessName(), objExcelTo.getBusinessTypeId(), objExcelTo.getLocationId(), objExcelTo.getMobileNumber(),
				objExcelTo.getStatus() };
		row = sheet.createRow(rowCount);
		for (int cellcount = 0; cellcount < arr.length; cellcount++) {
			Cell cell = row.createCell(cellcount);
			cell.setCellValue(arr[cellcount]);
		}
		FileOutputStream fos = new FileOutputStream(duplicatePhNoFile);
		workbook.write(fos);
		fos.close();
		System.out.println("Done");
	}

	public static void WritingExcelDuplicateData(ExcelTo objExcelTo) throws Exception {
		String duplicateData = "/home/vasudhaika/duplicateData.xlsx";
		File duplicateDataFile = new File(duplicateData);
		Workbook workbook;
		Sheet sheet;
		int rowCount = 0;
		if (duplicateDataFile.exists()) {
			FileInputStream fis = new FileInputStream(duplicateDataFile);
			workbook = new XSSFWorkbook(fis);
			sheet = workbook.getSheetAt(workbook.getNumberOfSheets() - 1);
			rowCount = sheet.getLastRowNum() + 1;
			if (rowCount == lastrowCount) {
				int sheetNumber = workbook.getNumberOfSheets() + 1;
				sheet = workbook.createSheet("Sheet" + sheetNumber);
				rowCount = 0;
			}
		} else {
			workbook = new XSSFWorkbook();
			sheet = workbook.createSheet("Sheet1");
			rowCount = 0;
		}
		Row row;
		String arr[] = new String[] { objExcelTo.getName(), objExcelTo.getBusinessName(), objExcelTo.getBusinessTypeId(), objExcelTo.getLocationId(), objExcelTo.getMobileNumber(),
				objExcelTo.getStatus() };
		row = sheet.createRow(rowCount);
		for (int cellcount = 0; cellcount < arr.length; cellcount++) {
			Cell cell = row.createCell(cellcount);
			cell.setCellValue(arr[cellcount]);
		}
		FileOutputStream fos = new FileOutputStream(duplicateDataFile);
		workbook.write(fos);
		fos.close();
		System.out.println("Done");

	}

	public static void WritingExcelDuplicateAll(ExcelTo objExcelTo) throws Exception {
		String duplicateAll = "/home/vasudhaika/duplicateAll.xlsx";
		File duplicateAllFile = new File(duplicateAll);
		Workbook workbook;
		Sheet sheet;
		int rowCount = 0;
		if (duplicateAllFile.exists()) {
			FileInputStream fis = new FileInputStream(duplicateAllFile);
			workbook = new XSSFWorkbook(fis);
			sheet = workbook.getSheetAt(workbook.getNumberOfSheets() - 1);
			rowCount = sheet.getLastRowNum() + 1;
			if (rowCount == lastrowCount) {
				int sheetNumber = workbook.getNumberOfSheets() + 1;
				sheet = workbook.createSheet("Sheet" + sheetNumber);
				rowCount = 0;
			}
		} else {
			workbook = new XSSFWorkbook();
			sheet = workbook.createSheet("Sheet1");
			rowCount = 0;
		}
		Row row;
		String arr[] = new String[] { objExcelTo.getName(), objExcelTo.getBusinessName(), objExcelTo.getBusinessTypeId(), objExcelTo.getLocationId(), objExcelTo.getMobileNumber(),
				objExcelTo.getStatus() };
		row = sheet.createRow(rowCount);
		for (int cellcount = 0; cellcount < arr.length; cellcount++) {
			Cell cell = row.createCell(cellcount);
			cell.setCellValue(arr[cellcount]);
		}
		FileOutputStream fos = new FileOutputStream(new File(duplicateAll));
		workbook.write(fos);
		fos.close();
		System.out.println("Done");
	}
}
