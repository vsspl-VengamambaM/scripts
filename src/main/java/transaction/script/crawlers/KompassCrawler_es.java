package transaction.script.crawlers;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class KompassCrawler_es {
	public static void main(String[] args) {
		String baseUrl = "http://in.kompass.com";
		Response response = null;
		String javaSessionId = "", language = "", routeId = "";
		Map<String, String> cookieDetails = new HashMap<String, String>();
		try {
			response = Jsoup
					.connect("http://in.kompass.com/searchCompanies?acClassif=&localizationCode=UA&localizationLabel=Ukraine&localizationType=country&text=food&searchType=ALL")
					.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").execute();
			javaSessionId = response.cookie("JSESSIONID");
			language = response.cookie("_k_cty_lang");
			routeId = response.cookie("ROUTEID");
			cookieDetails.put("JSESSIONID", javaSessionId);
			cookieDetails.put("_k_cty_lang", language);
			cookieDetails.put("ROUTEID", routeId);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Unable to initialise...");
			System.exit(1);
		}
		for (int i = 1; i <= 5; i++) {
			try {
				Element mainLink = response.parse().getElementById("resultatDivId");
				Response linksPageResponse = Jsoup.connect("http://in.kompass.com/searchCompanies/scroll?pageNbre=" + i).cookies(cookieDetails)
						.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").execute();
				Element linksPage = linksPageResponse.parse().getElementById("resultatDivId");
				for (Element element : linksPage.getElementsByClass("details")) {
					String subLinkUrl = baseUrl + element.getElementsByTag("h2").get(0).getElementsByTag("a").attr("href");
					Response subLinkPageResponse = Jsoup.connect(subLinkUrl).cookies(cookieDetails)
							.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").execute();
					Document subLinkDocument = subLinkPageResponse.parse();
					System.out.println(subLinkDocument);
				}
				System.exit(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}